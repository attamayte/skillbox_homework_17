#include "Vector3.h"


Vector3 Vector3::Normalize(const float x, const float y, const float z)
{
	if (LengthSq(x, y, z) < PRECISION)
		return Vector3{ 0.0F };
	// f = inverse square root factor
	float f = 1.0F / Length(x, y, z);
	
	return Vector3(x * f, y * f, z * f);
}


// basic implementation of Newton's sqrt
float Vector3::sqrt(const float x)
{
	float y = 1.0F, lim = x * PRECISION;

	while (abs((y * y) - x) > lim)
		y = (x / y + y) / 2.0F;

	return y;
}
