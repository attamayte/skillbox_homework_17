#pragma once
#include <iostream>

class Vector3
{
public:

	// ---------------------------------------------------------------------------
	//                     CONSTRUCTORS AND DESTRUCTORS
	// ---------------------------------------------------------------------------

	// default constructor
	Vector3()
		: x(0.0F), y(0.0F), z(0.0F) {}

	// (x) --> x, x, x
	Vector3(const float _x)
		: x(_x), y(_x), z(_x) {}

	// (x, y, z) --> x, y, z
	Vector3(const float _x, const float _y, const float _z)
		: x(_x), y(_y), z(_z) {}

	// copy constructor
	Vector3(const Vector3& vec)
		: x(vec.x), y(vec.y), z(vec.z) {}

	// default destructor
	~Vector3() {}

	// ---------------------------------------------------------------------------
	//                          GETTERS AND SETTERS
	// ---------------------------------------------------------------------------

	float GetX() const { return x; }
	float GetY() const { return y; }
	float GetZ() const { return z; }

	void SetX(const float _x) { x = _x; }
	void SetY(const float _y) { y = _y; }
	void SetZ(const float _z) { z = _z; }

	// ---------------------------------------------------------------------------
	//                          UTILITY FUNCTIONS
	// ---------------------------------------------------------------------------

	static const float LengthSq(const float x, const float y, const float z)
	{
		return x * x + y * y + z * z;
	}

	static const float LengthSq(const Vector3& vec)
	{
		return LengthSq(vec.x, vec.y, vec.z);
	}

	static const float Length(const float x, const float y, const float z)
	{
		return  sqrt(x * x + y * y + z * z);
	}

	static const float Length(const Vector3& vec)
	{
		return Length(vec.x, vec.y, vec.z);
	}

	static Vector3 Normalize(const float x, const float y, const float z);

	static Vector3 Normalize(const Vector3& vec)
	{ 
		return Normalize(vec.x, vec.y, vec.z);
	}

	// returns a Vector3 length in float
	float Length() const
	{
		return Length(x, y, z);
	}

	// UNSAFE: normalize Vector3 values
	Vector3& Normalize()
	{
		return *this = Normalize(x, y, z);
	}

	// SAFE: return a temp copy with normalized values
	Vector3 GetNormalized() const
	{
		return Normalize(x, y, z);
	}

	// ---------------------------------------------------------------------------
	//                          OPERATOR OVERLOADS
	// ---------------------------------------------------------------------------

	friend std::ostream& operator<<(std::ostream& os, const Vector3& vec)
	{
		return std::cout << '(' << vec.x << '|' << vec.y << '|' << vec.z << ')';
	}

	// for such a small value-type class checking for (this == rhs) is not necessary
	Vector3& operator=(const Vector3& rhs) 
	{
		x = rhs.x, y = rhs.y, z = rhs.z; return *this;
	}

	Vector3 operator+(const Vector3& rhs) const 
	{
		return Vector3(x + rhs.x, y + rhs.y, z + rhs.z);
	}

	Vector3 operator+(const float rhs) const 
	{
		return Vector3(x + rhs, y + rhs, z + rhs);
	}

	Vector3& operator+=(const Vector3& rhs)
	{
		x += rhs.x, y += rhs.y, z += rhs.z; return *this;
	}

	Vector3& operator+=(const float rhs)
	{
		x += rhs, y += rhs, z += rhs; return *this;
	}

	Vector3& operator*=(const Vector3& rhs) 
	{ 
		x *= rhs.x, y *= rhs.y, z *= rhs.z; return *this;
	}

	Vector3& operator*=(const float rhs) 
	{
		x *= rhs, y *= rhs, z *= rhs; return *this;
	}

	bool operator==(const Vector3& rhs)
	{
		return ((abs(x - rhs.x) < PRECISION) && (abs(y - rhs.y) < PRECISION) && (abs(z - rhs.z) < PRECISION)); 
	}

	bool operator!=(const Vector3& rhs)
	{
		return !(operator==(rhs)); 
	}

	// ---------------------------------------------------------------------------
	//                             INTERNAL STUFF
	// ---------------------------------------------------------------------------

protected:
	// abs and sqrt functions; didn't feel like include whole cmath.h just for two things
	static float abs(const float x) { return (x < 0.f ? -x : x); }
	static float sqrt(const float x);
	// internal precision control
	static constexpr float PRECISION = 0.0001F; // good enough for basic implementation

	// ---------------------------------------------------------------------------
	//                                  DATA
	// ---------------------------------------------------------------------------

private:
	float x, y, z;
	// float length; // might be nice to cache vector length
	// float normal; // and vector normal (normalized direction)
};
