#include "Vector3.h"

int main()
{
    Vector3 Position{}; // default constructor 0.0.0
    Vector3 Direction(14.0F, 0.0F, 0.0F); // kinda forward vector;
    Vector3 AnotherDirection(Direction + 30.0F);
    Vector3 LengthTest{ 7.0F, 200.0F, 40.0F };
    
    // little demonstration
    std::cout << "Test subject is located at " << Position << " and looks at " << Direction << '\n';
    std::cout << "Vector " << LengthTest << " length = " << LengthTest.Length() << '\n';
    std::cout << "Direction vector " << Direction << " and it's normalized copy " << Direction.GetNormalized() << '\n';
    std::cout << "LengthTest vector before " << LengthTest << " and after normalizing itself ";
    std::cout << LengthTest.Normalize() << '\n';
    std::cout << "Here goes AnotherDirection " << AnotherDirection << " with normalized (temp) value equal " << AnotherDirection.GetNormalized() << '\n';

    std::cout << '\n';

    if (Direction == AnotherDirection && Direction != Direction)
        std::cout << "Oops, we have a situation here, sarge!\n";
    else
        std::cout << "Operator== operates as intended, good work!\n";

    return 0;
}
